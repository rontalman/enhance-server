# Enhance prototype Microserver
The interface between the Unity game and ROS.

## Installation instructions
### Prerequisites
This software runs on Python 3. Be sure to have `python3` and `pip3` available.

On Ubuntu it'll be something like this:
>`sudo apt-get -y install python3-pip`

On Mac you can use `brew`. If `brew` isn't installed yet, install it like this:
>`ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
`

after which `python3` and `pip3` can be installed like this:
>`brew install python3`

Follow any of the instructions given. For example, `xcode-select` might have to be installed.

### Installation of the Enhance Microserver

Run the included `install.sh` script as such:
>`./install.sh`

This will download and install all required dependencies (found in `requirements.txt`).

### Running of the Microserver
#### Debug mode
To start the server in __debug__ mode, use the included `start-debug.sh` as such:
>`./start-debug.sh`

Alternatively, execute:

>`python3 main.py debug`

#### Production mode
To start the server in __production__ mode, use the included `start-production.sh` as such:
>`./start-production.sh`

Alternatively, execute:

>`python3 main.py`

### Troubleshooting
Be sure to open up port `5432` to the server.

If the connection still cannot be made, check the firewall.

If the connection _still_ cannot be made, try changing the `host` variable in `main.py`. It defaults to `0.0.0.0` now, because `127.0.0.1` doesn't allow external connections on Mac/Unix. Try setting this host to the current local IP, perhaps using `ifconfig` to find the local IP.