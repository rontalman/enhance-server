from flask import abort, Flask, jsonify, request

import sys

app = Flask(__name__)


# Microserver for the Enhance project
# Interfaces Unity with ROS

# All vectors are received and should be returned in Unity world space (middle of the screen is the origin (0, 0, 0))
# All requests receive a (unique) token for recognition purposes in case the asynchronous nature of the calls
# All requests should return 200 on success

@app.route("/", methods=['GET'])
def ping():
    # For testing purposes

    return jsonify({"ping": "pong"}), 200


@app.route("/robot/arm/status", methods=['GET'])
def robot_arm_status():
    if not (request.args or "token" in request.args):
        abort(400)

    # Please add the ROS code here..
    # status: moving    - if the arm is still in use
    # status: idle      - if the arm is back at its start position

    return jsonify({"status": "idle", "token": request.args.get("token")}), 200


@app.route("/robot/arm/position", methods=['GET'])
def robot_arm_position():
    if not request.args and "token" in request.args:
        abort(400)

    # Please add the ROS code here..
    # position: {x: 0.1, y: 0.2, z: 0.3} - position of the arm in unity coordinates

    return jsonify({"position": {"x": 0, "y": 0, "z": 0}, "token": request.args.get("token")}), 200
    # return jsonify({"position": {"x": 1, "y": 1, "z": 0}, "token": request.args.get("token")}), 200
    # return jsonify({"position": {"x": -4, "y": -6, "z": 0}, "token": request.args.get("token")}), 200
    # return jsonify({"position": {"x": 1.2424, "y": 5.1231, "z": -1.1423}, "token": request.args.get("token")}), 200


# Example of data to receive
# {
# 	"identifier": "uniqueidentifier",
# 	"precision": 1
# }
@app.route("/robot/arm/precision", methods=['PUT'])
def robot_arm_precision():
    if not ((request.args and "token" in request.args) and (request.json and "precision" in request.json)):
        abort(400)

    # Retrieve the position from the received data
    precision = request.json.get('precision')
    # 0 = in the inner ring
    # 1 = in the outer ring
    # 2 = outside of the ring
    print(precision)
    # Please add the ROS code here..

    # Return a success: True if all goes well
    return jsonify({"success": True, "token": request.args.get("token")}), 200


# Example of data to receive
# {
# 	"identifier": "uniqueidentifier",
# 	"position": {"x": 1.0, "y": 2.0, "z": 3.0}
# }
@app.route("/robot/arm/target", methods=['PUT'])
def robot_arm_target():
    if not ((request.args and "token" in request.args) and (request.json and "position" in request.json)):
        abort(400)

    # Retrieve the position from the received data
    position = request.json.get('position')

    # Access the x, y, z variables as such
    print(position["x"])
    print(position["y"])
    print(position["z"])

    # Please add the ROS code here..

    # Return a success: True if all goes well
    return jsonify({"success": True, "token": request.args.get("token")}), 200


if __name__ == '__main__':
    port = 5432
    if len(sys.argv) == 2:
        app.run(debug=sys.argv[1] == 'debug', host='0.0.0.0', port=port)
    else:
        app.run(host='0.0.0.0', port=port)
